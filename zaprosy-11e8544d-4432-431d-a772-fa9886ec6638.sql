--1) Вывести всё из таблицы Animal
SELECT * FROM animal;

--2)  Вывести записи из таблицы Animal только мужского пола (sex = 1)
SELECT * FROM animal
WHERE sex=1;

--3) Вывести все записи из таблицы Animal, которые имеют тип 1 и 2
SELECT * FROM animal
WHERE type in (1,2);

--4)  Вывести имена всех животных и названия их типов
SELECT a.name AS animal_name, t."name" AS type_name
FROM animal a
INNER JOIN types t ON a.type = t.id;

--5) Вывести имена всех животных и имена их рабочих
SELECT a.name AS animal_name, w.name AS workman_name
FROM animal a
INNER JOIN zoo_animal za ON a.id = za.animal_id
INNER JOIN workman w ON za.workman = w.id;

--6) Вывести названия зоопарков, в которых есть рабочие меньше 50 лет. Названия должны быть выведены только уникальные.
SELECT DISTINCT z.name 
FROM zoo z
INNER JOIN zoo_animal za ON z.id = za.zoo_id
INNER JOIN workman w ON za.workman = w.id
WHERE w.age < 50;

--7) Вывести названия зоопарков и общий возраст находящихся в них животных.
SELECT z.name, SUM(a.age) AS total_age
FROM zoo z
INNER JOIN zoo_animal za ON z.id = za.zoo_id
INNER JOIN animal a ON za.animal_id = a.id
GROUP BY z.name;

--8) Вывести название зоопарка, в котором нет ни одного животного.
SELECT z.name
FROM zoo z
LEFT JOIN zoo_animal za ON z.id = za.zoo_id
WHERE za.animal_id IS NULL;

--9) Вывести имена животных и названия их загонов, отсортировав названия загонов по убыванию.
SELECT a.name AS animal_name, p.name AS place_name
FROM animal a
INNER JOIN places p ON a.place = p.id
ORDER BY p.name DESC;

--10) Вывести название зоопарка, имя работника, название должности и его зарплату.Отсортировать по уровню зарплат и вывести топ-3 строки с самыми высокими.
SELECT z.name AS zoo_name, w.name AS workman_name, p.name AS position_name, p.salary
FROM zoo z
INNER JOIN zoo_animal za ON z.id = za.zoo_id
INNER JOIN workman w ON za.workman = w.id
INNER JOIN positions p ON w."position" = p.id
ORDER BY p.salary DESC
LIMIT 3;